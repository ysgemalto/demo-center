#importing required packages
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from soaps import *


@csrf_exempt
def display(request):

    if request.method == 'POST':


        offer = 'demo_Sma_Smt_T5345'
        tel = request.POST.get('tel')
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')

        msisdn = tel
        custom1 = name
        custom2 = email
        custom3 = phone

        response = three_variable_campaign(offer, msisdn, custom1, custom2, custom3)

        context = {
            'tel': tel,
            'name': name,
            'email': email,
            'phone': phone
        }

        template = loader.get_template('showdata.html')

        print(response)

        return HttpResponse(template.render(context, request))
    else:

        template = loader.get_template('index.html')
        return HttpResponse(template.render())

@csrf_exempt
def index(request):

    if request.method == 'POST':

        offer = 'demo_Sma_Smt_T5345'
        tel = request.POST.get('tel')
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')

        msisdn = tel
        custom1 = name
        custom2 = email
        custom3 = phone

        response = three_variable_campaign(offer, msisdn, custom1, custom2, custom3)

        context = {
            'tel': tel,
            'name': name,
            'email': email,
            'phone': phone
        }

        template = loader.get_template('showdata.html')

        return HttpResponse(template.render(context, request))
    else:

        template = loader.get_template('base.html')
        return HttpResponse(template.render())