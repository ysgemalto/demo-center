import requests

# demo_Sma_Smt_T5345


def three_variable_campaign(offer ,msisdn, custom1, custom2, custom3):
    url = 'http://uc.smartmessage.gemalto.com:8585/SmartMessageDispatcher/Service'
    headers = {'content-type': 'text/xml'}

    body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gem="http://gemalto.com">
       <soapenv:Header />
       <soapenv:Body>
          <gem:SendUnitaryRequest>
             <gem:params>
                <gem:api-key>afee781c9feba9e1f9be5eb1c3564824d126b64abb1ae09673ce1dacc844027d</gem:api-key>
                <gem:scenario-type>OFFER</gem:scenario-type>
                
                <gem:offer-reference>%s</gem:offer-reference>
                <gem:target address="+55%s">
                   <gem:attribute name="Custom1" value="%s" />
                   <gem:attribute name="Custom2" value="%s" />
                   <gem:attribute name="Custom3" value="%s" />
                </gem:target>
             </gem:params>
          </gem:SendUnitaryRequest>
       </soapenv:Body>
    </soapenv:Envelope>""" % (offer, msisdn, custom1, custom2, custom3)

    r = requests.post (url, data=body, headers=headers)
    r.status_code
    return r.status_code


def two_variable_campaign(offer, msisdn, custom1, custom2):
    url = 'http://uc.smartmessage.gemalto.com:8585/SmartMessageDispatcher/Service'
    headers = {'content-type': 'text/xml'}

    body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gem="http://gemalto.com">
       <soapenv:Header />
       <soapenv:Body>
          <gem:SendUnitaryRequest>
             <gem:params>
                <gem:api-key>afee781c9feba9e1f9be5eb1c3564824d126b64abb1ae09673ce1dacc844027d</gem:api-key>
                <gem:scenario-type>OFFER</gem:scenario-type>
                <gem:offer-reference>%s</gem:offer-reference>
                <gem:target address="+55%s">
                   <gem:attribute name="Custom1" value="%s" />
                   <gem:attribute name="Custom2" value="%s" />
                </gem:target>
             </gem:params>
          </gem:SendUnitaryRequest>
       </soapenv:Body>
    </soapenv:Envelope>""" % (offer, msisdn, custom1, custom2)

    r = requests.post (url, data=body, headers=headers)
    r.status_code
    return r.status_code


def one_variable_campaign(offer, msisdn, custom1):
    url = 'http://uc.smartmessage.gemalto.com:8585/SmartMessageDispatcher/Service'
    headers = {'content-type': 'text/xml'}

    body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gem="http://gemalto.com">
       <soapenv:Header />
       <soapenv:Body>
          <gem:SendUnitaryRequest>
             <gem:params>
                <gem:api-key>afee781c9feba9e1f9be5eb1c3564824d126b64abb1ae09673ce1dacc844027d</gem:api-key>
                <gem:scenario-type>OFFER</gem:scenario-type>
                <gem:offer-reference>%s</gem:offer-reference>
                <gem:target address="+55%s">
                   <gem:attribute name="Custom1" value="%s" />
                </gem:target>
             </gem:params>
          </gem:SendUnitaryRequest>
       </soapenv:Body>
    </soapenv:Envelope>""" % (offer, msisdn, custom1)

    r = requests.post (url, data=body, headers=headers)
    r.status_code
    return r.status_code


def no_variable_campaign(offer, msisdn):
    url = 'http://uc.smartmessage.gemalto.com:8585/SmartMessageDispatcher/Service'
    headers = {'content-type': 'text/xml'}

    body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gem="http://gemalto.com">
       <soapenv:Header />
       <soapenv:Body>
          <gem:SendUnitaryRequest>
             <gem:params>
                <gem:api-key>afee781c9feba9e1f9be5eb1c3564824d126b64abb1ae09673ce1dacc844027d</gem:api-key>
                <gem:scenario-type>OFFER</gem:scenario-type>
                <gem:offer-reference>%s</gem:offer-reference>
                <gem:target address="+55%s">
                </gem:target>
             </gem:params>
          </gem:SendUnitaryRequest>
       </soapenv:Body>
    </soapenv:Envelope>""" % (offer, msisdn)

    r = requests.post (url, data=body, headers=headers)
    r.status_code
    return r.status_code
